<?php
namespace Spec;

define('APPDIR', dirname(__FILE__).'/..');
require APPDIR.'/vendor/autoload.php';
require APPDIR.'/curl.php';

describe('Curl', function () {
  given('curl', function() {
    return new \Curl([], []);
  });

  describe('instance', function () {
    it('return "Curl" instance', function () {
      expect($this->curl)->toBeAnInstanceOf(\Curl::class);
    });
  });

  describe("Asynchronous Expectations", function() {
    \Curl::init();

    it('testing a valid website', function () {

       expect(
         $this->curl->getHttpCode('https://kahlan.github.io/docs/index.html')
         ->then(function($httpCode){
          //
           expect($httpCode)->toBe(200);
           return $httpCode;
         })
       )->toBeAnInstanceOf(\React\Promise\Promise::class);

       \Curl::run();
     });

     it('testing a invalid website', function () {

        expect(
          $this->curl->getHttpCode('https://kahlan.github.io')
          ->then(null,function($httpCode){
           //
            expect($httpCode)->toBe("-1");
            return $httpCode;
          })
        )->toBeAnInstanceOf(\React\Promise\Promise::class);

        \Curl::run();
      });
  });
});
