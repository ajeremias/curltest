<?php

use \KHR\React;
use \KHR\React\Curl\Exception;
use \React\EventLoop\Factory;
use \React\Promise\Promise;

class Curl{
  protected static $loop;
  public static $curlInstance;

  function __construct()
  {
  }

  static function init(){
    if(is_null(self::$loop)){
      self::$loop = Factory::create();
    }
    if(is_null(self::$curlInstance)){
      self::$curlInstance = new React\Curl\Curl(self::$loop);
      self::$curlInstance->client->setCurlOption([
        CURLOPT_FRESH_CONNECT => true,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_MAXREDIRS => 3,
        CURLOPT_HEADER => true,
        CURLOPT_NOBODY => true,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_CUSTOMREQUEST => 'HEAD'
      ]);
    }
  }
  static function run() {
    self::$loop->run();
  }

  static function getHttpCode($uri){
    $resolver = function (callable $resolve, callable $reject) use ($uri){
      // $cuurl = new React\Curl\Curl(self::$loop);
      // $cuurl
      self::$curlInstance
      ->get($uri)->then(function($result) use ($resolve){
        $httpCode = curl_getinfo($result->ch, CURLINFO_HTTP_CODE);
        $resolve($httpCode);
      },function($err) use ($reject){
        $reject("-1");
      });

    };
    return new Promise($resolver);
  }
}