<?php
define('APPDIR', dirname(__FILE__));
require APPDIR.'/vendor/autoload.php';
require APPDIR.'/curl.php';

$url = "https://duckduckgo.com";
\Curl::init();

for($i=0;$i < 100; $i++){
  \Curl::getHttpCode($url)
  ->then(function($httpCode) use ($url, $i){
    echo "[$i] $url $httpCode\n";
  },function($err){
    echo $err;
  });
}

\Curl::run();